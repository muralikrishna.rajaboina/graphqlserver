var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/test', { useNewUrlParser: true, useUnifiedTopology: true });
const Schema = mongoose.Schema

const productDetailsSchema = new Schema({
  productId: Number,
  available_quantity: Number,
  sold_quantity: Number,
  damaged_quantity: Number
})

const Model = mongoose.model
const Product = Model('ProductDetails', productDetailsSchema)

/* GET home page. */
router.post('/insert', function (req, res, next) {
  const product = new Product({
    productId: 2,
    available_quantity: "20",
    sold_quantity: "25",
    damaged_quantity: "15"
  })
  product.save((err, result) => {
    if (err) console.log(err)
    res.send(result)
  })
});

router.get("/productdetails", async (req, res) => {
  const producdetails = await Product.find({}, { _id: 0, __v: 0 })
  res.send(producdetails)
})

module.exports = router;
